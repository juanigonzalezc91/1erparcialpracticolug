USE [master]
GO
/****** Object:  Database [1erParcialLUG]    Script Date: 26/9/2020 01:26:26 ******/
CREATE DATABASE [1erParcialLUG]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'1erParcialLUG', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\1erParcialLUG.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'1erParcialLUG_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\1erParcialLUG_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [1erParcialLUG] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [1erParcialLUG].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [1erParcialLUG] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [1erParcialLUG] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [1erParcialLUG] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [1erParcialLUG] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [1erParcialLUG] SET ARITHABORT OFF 
GO
ALTER DATABASE [1erParcialLUG] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [1erParcialLUG] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [1erParcialLUG] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [1erParcialLUG] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [1erParcialLUG] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [1erParcialLUG] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [1erParcialLUG] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [1erParcialLUG] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [1erParcialLUG] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [1erParcialLUG] SET  DISABLE_BROKER 
GO
ALTER DATABASE [1erParcialLUG] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [1erParcialLUG] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [1erParcialLUG] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [1erParcialLUG] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [1erParcialLUG] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [1erParcialLUG] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [1erParcialLUG] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [1erParcialLUG] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [1erParcialLUG] SET  MULTI_USER 
GO
ALTER DATABASE [1erParcialLUG] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [1erParcialLUG] SET DB_CHAINING OFF 
GO
ALTER DATABASE [1erParcialLUG] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [1erParcialLUG] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [1erParcialLUG] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [1erParcialLUG] SET QUERY_STORE = OFF
GO
USE [1erParcialLUG]
GO
/****** Object:  Table [dbo].[CONCEPTO]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCEPTO](
	[IDConcepto] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[Coeficiente] [decimal](18, 5) NOT NULL,
	[Activo] [varchar](1) NOT NULL,
	[Operador] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_CONCEPTO] PRIMARY KEY CLUSTERED 
(
	[IDConcepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DETALLE_RECIBO]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DETALLE_RECIBO](
	[Legajo] [int] NOT NULL,
	[Ano] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[IDConcepto] [int] NOT NULL,
 CONSTRAINT [PK_DETALLE_RECIBO_1] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC,
	[Ano] ASC,
	[Mes] ASC,
	[IDConcepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMPLEADO]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMPLEADO](
	[Legajo] [int] NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[Apellido] [nvarchar](50) NULL,
	[CUIL] [nchar](13) NULL,
	[FechaIngreso] [datetime] NULL,
 CONSTRAINT [PK_EMPLEADO] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECIBO]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECIBO](
	[Legajo] [int] NOT NULL,
	[Ano] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[SueldoBruto] [decimal](18, 2) NOT NULL,
	[Pagado] [nvarchar](1) NULL,
	[SueldoNeto] [decimal](18, 2) NULL,
 CONSTRAINT [PK_RECIBO] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC,
	[Ano] ASC,
	[Mes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DETALLE_RECIBO]  WITH CHECK ADD  CONSTRAINT [FK_DETALLE_RECIBO_CONCEPTO] FOREIGN KEY([IDConcepto])
REFERENCES [dbo].[CONCEPTO] ([IDConcepto])
GO
ALTER TABLE [dbo].[DETALLE_RECIBO] CHECK CONSTRAINT [FK_DETALLE_RECIBO_CONCEPTO]
GO
ALTER TABLE [dbo].[DETALLE_RECIBO]  WITH CHECK ADD  CONSTRAINT [FK_DETALLE_RECIBO_RECIBO1] FOREIGN KEY([Legajo], [Ano], [Mes])
REFERENCES [dbo].[RECIBO] ([Legajo], [Ano], [Mes])
GO
ALTER TABLE [dbo].[DETALLE_RECIBO] CHECK CONSTRAINT [FK_DETALLE_RECIBO_RECIBO1]
GO
ALTER TABLE [dbo].[RECIBO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_EMPLEADO] FOREIGN KEY([Legajo])
REFERENCES [dbo].[EMPLEADO] ([Legajo])
GO
ALTER TABLE [dbo].[RECIBO] CHECK CONSTRAINT [FK_RECIBO_EMPLEADO]
GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_BORRAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[CONCEPTO_BORRAR]
@id int
as
begin

update CONCEPTO set Activo = 'X' where IDConcepto = @id

end

GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_EDITAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[CONCEPTO_EDITAR]
@id int, @desc nvarchar(50), @coef decimal(18,5), @op nvarchar(1)
as
begin


update CONCEPTO set Descripcion = @desc, Coeficiente = @coef, Operador = @op where IDConcepto = @id

end

GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_INSERTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[CONCEPTO_INSERTAR]
@desc nvarchar(50), @coef decimal(18,5), @operador nvarchar(1)
as
begin

declare @id int

set @id = ISNULL((select max(IDConcepto) from CONCEPTO),0)+1

insert into CONCEPTO (IDConcepto, Descripcion,Coeficiente,Activo, Operador) values (@id, @desc, @coef, '', @operador)


end

GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_LISTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CONCEPTO_LISTAR]	

as
begin

select c.IDConcepto, c.Descripcion, c.Coeficiente, c.Operador
from CONCEPTO C
where c.Activo = ''

end

GO
/****** Object:  StoredProcedure [dbo].[CONCEPTO_RECIBO_LISTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CONCEPTO_RECIBO_LISTAR]
@id int, @ano int, @mes int
as
begin
select D.IDConcepto, C.Descripcion, C.Coeficiente, C.Operador
from  DETALLE_RECIBO D inner join CONCEPTO C on D.IDConcepto = C.IDConcepto 
where D.Legajo = @id AND D.Ano = @ano AND D.Mes = @mes;

end
GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_BORRAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[EMPLEADO_BORRAR]
@id int
as 
begin
	delete from DETALLE_RECIBO where Legajo = @id
	delete from RECIBO where Legajo = @id
	delete from EMPLEADO where Legajo = @id

	
	
	

end

GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_EDITAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[EMPLEADO_EDITAR]
@id int, @nom nvarchar(50), @ape nvarchar(50), @cuil nvarchar(13)
as 
begin
	
	update EMPLEADO set
	Nombre = @nom,
	Apellido = @ape,
	CUIL = @cuil 
	where Legajo = @id
	

end

GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_INSERTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[EMPLEADO_INSERTAR]
@nom nvarchar(50), @ape nvarchar(50), @cuil nvarchar(13)
as 
begin
	Declare @id int

	set @id = ISNULL((select max (Legajo) from EMPLEADO),0) +1

	insert into EMPLEADO (Legajo, Nombre, Apellido, CUIL, FechaIngreso)
	values	(@id, @nom, @ape, @cuil, GETDATE())

end

GO
/****** Object:  StoredProcedure [dbo].[EMPLEADO_LISTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[EMPLEADO_LISTAR]
as 
begin


SELECT E.Legajo, e.Nombre, e.Apellido, e.CUIL, e.FechaIngreso
FROM EMPLEADO E


end

GO
/****** Object:  StoredProcedure [dbo].[LIQUIDAR_RECIBO]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[LIQUIDAR_RECIBO]
@id int, @ano int, @mes int
as
begin
declare @neto decimal(18,2)
declare @descuentos decimal (18,2)
declare @bruto decimal (18,2)
declare @netoaux decimal(18,2)

set @bruto = (select R.SueldoBruto from RECIBO R where R.Legajo = @id AND R.Ano = @ano AND R.Mes = @mes)
set @descuentos = (
SELECT sum("Monto") as "Monto" FROM (
select 
R.Legajo, 
R.Ano, 
R.Mes, 
R.SueldoBruto, 
C.Descripcion, 
(case when C.Operador = 'S' then C.Coeficiente * 100 else C.Coeficiente*100*-1 end) as "Porcentaje aplicado", 
(case when C.Operador = 'S' then R.SueldoBruto*C.Coeficiente else R.SueldoBruto*C.Coeficiente*-1 end) as "Monto"

from RECIBO R
inner join DETALLE_RECIBO D on (R.Legajo = D.Legajo AND R.Ano = D.Ano AND R.Mes = D.Mes)
inner join CONCEPTO C on D.IDConcepto = C.IDConcepto
where (R.Legajo = @id AND R.Ano = @ano AND R.Mes = @mes)
) as "Monto"

)
set @neto = @bruto + @descuentos
set @netoaux = @neto
if (@neto IS NULL) set @netoaux = @bruto



update RECIBO  set SueldoNeto = @netoaux, Pagado = 'X' where (Legajo = @id AND Ano = @ano AND Mes = @mes)



end
GO
/****** Object:  StoredProcedure [dbo].[ListarID]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarID]
@id int
as 
begin

select Legajo, Nombre, Apellido, CUIL, FechaIngreso
from EMPLEADO
where Legajo = @id


end
GO
/****** Object:  StoredProcedure [dbo].[RECIBO_CONCEPTO]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[RECIBO_CONCEPTO]
@id int, @ano int, @mes int, @idconc int
as
begin

declare @valido int

set @valido = (select COUNT(IDConcepto) from CONCEPTO where IDConcepto = @idconc AND Activo = '')

if(@valido = 1)
begin
insert into DETALLE_RECIBO (Legajo, Ano, Mes, IDConcepto ) values (@id, @ano, @mes, @idconc)
end




end

GO
/****** Object:  StoredProcedure [dbo].[RECIBO_INSERTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[RECIBO_INSERTAR]
@id int, @ano int, @mes int, @sueldo decimal(18,2)
as
begin

insert into RECIBO (Legajo, Ano, Mes, SueldoBruto, Pagado) values (@id, @ano, @mes, @sueldo, '')



end

GO
/****** Object:  StoredProcedure [dbo].[RECIBO_LISTAR]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[RECIBO_LISTAR]	
@id int, @ano int, @mes int	
as
begin

select 
C.Descripcion, 
(case when C.Operador = 'S' then C.Coeficiente * 100 else C.Coeficiente*100*-1 end) as "Porcentaje aplicado", 
(case when C.Operador = 'S' then R.SueldoBruto*C.Coeficiente else R.SueldoBruto*C.Coeficiente*-1 end) as "Monto"

from RECIBO R
left join DETALLE_RECIBO D on (R.Legajo = D.Legajo AND R.Ano = D.Ano AND R.Mes = D.Mes)
left join CONCEPTO C on D.IDConcepto = C.IDConcepto
where (R.Legajo = @id AND R.Ano = @ano AND R.Mes = @mes)
end

GO
/****** Object:  StoredProcedure [dbo].[RECIBO_LISTAR_TODOS]    Script Date: 26/9/2020 01:26:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[RECIBO_LISTAR_TODOS]	
@id int	
as
begin

select 
R.Legajo, 
R.Ano, 
R.Mes, 
R.SueldoBruto,
(case when ISNULL(R.Pagado,'') = '' then 'Pendiente' else 'Liquidado' end) as 'Estado',
(case when ISNULL(R.Pagado,'') = '' then '0' else R.SueldoNeto end) as 'Monto abonado'


from RECIBO R

where (R.Legajo = @id)
end

GO
USE [master]
GO
ALTER DATABASE [1erParcialLUG] SET  READ_WRITE 
GO
