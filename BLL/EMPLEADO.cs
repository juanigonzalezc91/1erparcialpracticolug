﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;


namespace BLL
{

    public class EMPLEADO
    {

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string cuil;

        public string CUIL
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaAlta;

        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }

        public override string ToString()
        {
            return legajo.ToString() + " - " + apellido + ", " + nombre;
        }

        private DAL.Acceso acceso = new Acceso();

        public int Insertar()
        {
            int filasafectadas;
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nom", this.nombre));
            parametros.Add(acceso.CrearParametro("@ape", this.apellido));
            parametros.Add(acceso.CrearParametro("@cuil", this.cuil));
            filasafectadas = acceso.Escribir("EMPLEADO_INSERTAR", parametros);                                 
            acceso.Cerrar();

            return filasafectadas;
        }

        public void Editar()
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", this.legajo));
            parametros.Add(acceso.CrearParametro("@nom", this.nombre));
            parametros.Add(acceso.CrearParametro("@ape", this.apellido));
            parametros.Add(acceso.CrearParametro("@cuil", this.cuil));
            acceso.Escribir("EMPLEADO_EDITAR", parametros);
            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", this.legajo));
            acceso.Escribir("EMPLEADO_BORRAR", parametros);
            acceso.Cerrar();
        }

        

        public static List<EMPLEADO> Listar()
        {
            List<EMPLEADO> lista = new List<EMPLEADO>();
            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("EMPLEADO_LISTAR");
            acceso.Cerrar();

            foreach(DataRow registro in tabla.Rows)
            {
                EMPLEADO e = new EMPLEADO();
                e.nombre = registro[1].ToString();
                e.legajo = int.Parse(registro[0].ToString());
                e.apellido = registro[2].ToString();
                e.cuil = registro[3].ToString();
                e.fechaAlta = DateTime.Parse(registro[4].ToString());
                lista.Add(e);
            }
            return lista;


        }

        public static List<EMPLEADO> ListarID(int id)
        {
            List<EMPLEADO> lista = new List<EMPLEADO>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", id));

            DataTable tabla = acceso.Leer("ListarID", parametros);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                EMPLEADO e = new EMPLEADO();
                e.nombre = registro[1].ToString();
                e.legajo = int.Parse(registro[0].ToString());
                e.apellido = registro[2].ToString();
                e.cuil = registro[3].ToString();
                e.fechaAlta = DateTime.Parse(registro[4].ToString());
                lista.Add(e);
            }
            return lista;


        }


    }
   
}
