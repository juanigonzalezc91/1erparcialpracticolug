﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;

namespace BLL
{
    public class RECIBO
    {
        public RECIBO ()
        {
            conceptos = new List<CONCEPTO>();
        }

        

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        private int mes;

        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }

        private float bruto;

        public float Bruto
        {
            get { return bruto; }
            set { bruto = value; }
        }

        private float neto;

        public float Neto
        {
            get { return neto; }
            set { neto = value; }
        }

        private string pagado;

        public string Pagado
        {
            get { return pagado; }
            set { pagado = value; }
        }

        private EMPLEADO empleado;

        public EMPLEADO Empleado
        {
            get { return empleado; }
            set { empleado = value; }
        }

        private List<CONCEPTO> conceptos;
                
        public List<CONCEPTO> Conceptos
        {
            get { return conceptos; }
            set { conceptos = value; }
        }

        public float TotalDescuentos()
        {
            int escalar;
            float descuentos = 0;
            foreach (CONCEPTO C in Conceptos)
            {
                if(C.Operador == "R")
                {
                    escalar = -1;
                }
                else
                {
                    escalar = 1;
                }

                descuentos += bruto * C.Coeficiente * escalar;
            }

            return descuentos;
        }

        private DAL.Acceso acceso = new Acceso();

        public int Insertar()
        {
            int filas;
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            

            parametros.Add(acceso.CrearParametro("@id", this.legajo));
            parametros.Add(acceso.CrearParametro("@ano", this.ano));
            parametros.Add(acceso.CrearParametro("@mes", this.mes));
            parametros.Add(acceso.CrearParametro("@sueldo", this.bruto));
            filas = acceso.Escribir("RECIBO_INSERTAR", parametros);

            acceso.Cerrar();
            return filas;
        }

        public void InsertarConceptos(List<CONCEPTO> conceptos)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametrosconc = new List<IDbDataParameter>();
            

            

            foreach (CONCEPTO C in conceptos)
            {
                parametrosconc.Add(acceso.CrearParametro("@id", this.legajo));
                parametrosconc.Add(acceso.CrearParametro("@ano", this.ano));
                parametrosconc.Add(acceso.CrearParametro("@mes", this.mes));
                parametrosconc.Add(acceso.CrearParametro("@idconc", C.ID));
                acceso.Escribir("RECIBO_CONCEPTO", parametrosconc);
                parametrosconc.Clear();
            }


            acceso.Cerrar();
        }

        public void Liquidar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();


            parametros.Add(acceso.CrearParametro("@id", this.legajo));
            parametros.Add(acceso.CrearParametro("@ano", this.ano));
            parametros.Add(acceso.CrearParametro("@mes", this.mes));
            acceso.Escribir("LIQUIDAR_RECIBO", parametros);


            acceso.Cerrar();
        }

        public static List<RECIBO> Listar(int ID)
        {
            List<RECIBO> lista = new List<RECIBO>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();


            parametros.Add(acceso.CrearParametro("@id", ID));

            DataTable tabla = acceso.Leer("RECIBO_LISTAR_TODOS", parametros);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                

                RECIBO R = new RECIBO();
                R.legajo = int.Parse(registro[0].ToString());
                R.ano = int.Parse(registro[1].ToString());
                R.mes = int.Parse(registro[2].ToString());
                R.bruto = float.Parse(registro[3].ToString());
                if (float.TryParse(registro[5].ToString(), out float N) == true)
                {
                    R.neto = N;
                }
                R.pagado = registro[4].ToString();
                
                lista.Add(R);
            }
            return lista;


        }

        public static List<CONCEPTO> ListarConc(int ID, int ANO, int MES)
        {
            List<CONCEPTO> lista = new List<CONCEPTO>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();


            parametros.Add(acceso.CrearParametro("@id", ID));
            parametros.Add(acceso.CrearParametro("@ano", ANO));
            parametros.Add(acceso.CrearParametro("@mes", MES));

            DataTable tabla = acceso.Leer("CONCEPTO_RECIBO_LISTAR", parametros);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {


                CONCEPTO C = new CONCEPTO();
                C.ID = int.Parse(registro[0].ToString());
                C.Descripcion = registro[1].ToString();
                C.Coeficiente = float.Parse(registro[2].ToString());
                C.Operador = registro[3].ToString();



                

                lista.Add(C);
            }
            return lista;


        }

        public static List<ITEM> ListarItem(int ID, int ANO, int MES)
        {
            List<ITEM> lista = new List<ITEM>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();


            parametros.Add(acceso.CrearParametro("@id", ID));
            parametros.Add(acceso.CrearParametro("@ano", ANO));
            parametros.Add(acceso.CrearParametro("@mes", MES));

            DataTable tabla = acceso.Leer("RECIBO_LISTAR", parametros);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {


                ITEM I = new ITEM();

                I.Concepto = registro[0].ToString();
                I.Porcentaje = float.Parse(registro[1].ToString());
                I.Monto = float.Parse(registro[2].ToString());

                lista.Add(I);
            }
            return lista;


        }










    }
}
