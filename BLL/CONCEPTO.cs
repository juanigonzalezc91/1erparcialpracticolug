﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;

namespace BLL
{
    public class CONCEPTO
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private string operador;

        public string Operador
        {
            get { return operador; }
            set { operador = value; }
        }

        private float coeficiente;

        public float Coeficiente
        {
            get { return coeficiente; }
            set { coeficiente = value; }
        }

        public override string ToString()
        {
            string signo;
            if(operador == "S")
            {
                signo = "+";
            }
            else
            {
                signo = "-";
            }
            return this.descripcion + " | " + signo + " " + (coeficiente * 100).ToString() + "%";
        }

        private DAL.Acceso acceso = new Acceso();

        public int Insertar()
        {
            int filasafectadas;
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@desc", this.descripcion));
            parametros.Add(acceso.CrearParametro("@coef", this.coeficiente));
            parametros.Add(acceso.CrearParametro("@operador", this.operador));
            filasafectadas = acceso.Escribir("CONCEPTO_INSERTAR", parametros);
            acceso.Cerrar();

            return filasafectadas;
        }

        public void Editar()
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id",this.id));
            parametros.Add(acceso.CrearParametro("@desc", this.descripcion));
            parametros.Add(acceso.CrearParametro("@coef", this.coeficiente));
            parametros.Add(acceso.CrearParametro("@op", this.operador));
            acceso.Escribir("CONCEPTO_EDITAR", parametros);
            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", this.id));
            acceso.Escribir("CONCEPTO_BORRAR", parametros);
            acceso.Cerrar();
        }



        public static List<CONCEPTO> Listar()
        {
            List<CONCEPTO> lista = new List<CONCEPTO>();
            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("CONCEPTO_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                CONCEPTO e = new CONCEPTO();
                e.descripcion = registro[1].ToString();
                e.id = int.Parse(registro[0].ToString());
                e.coeficiente = float.Parse(registro[2].ToString());
                e.operador = registro[3].ToString();
                lista.Add(e);
            }
            return lista;


        }



    }
}
