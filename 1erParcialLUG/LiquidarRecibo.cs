﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace _1erParcialLUG
{
    public partial class LiquidarRecibo : Form
    {
        public LiquidarRecibo()
        {
            InitializeComponent();
        }
        


        private void LiquidarRecibo_Load(object sender, EventArgs e)
        {
            EnlazarCombo();
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = RECIBO.Listar(((EMPLEADO)comboBox1.SelectedItem).Legajo);
        }

        private void EnlazarCombo()
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = EMPLEADO.Listar();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                Enlazar();
                lblID.Text = ".";
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            lblID.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            lblAno.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            lblMes.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            lblEstado.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            lblBruto.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lblID.Text == ".")
            {
                MessageBox.Show("Seleccione un recibo para liquidar");
            }
            else
            {
                RECIBO gestor = new RECIBO();
                gestor.Legajo = int.Parse(lblID.Text);
                gestor.Ano = int.Parse(lblAno.Text);
                gestor.Mes = int.Parse(lblMes.Text);
                gestor.Pagado = lblEstado.Text;

                if (gestor.Pagado == "Pendiente")
                {
                    gestor.Liquidar();

                    gestor = null;
                    GC.Collect();
                    Enlazar();
                    MessageBox.Show("El recibo se liquidó con éxito");
                    lblEstado.Text = "Liquidado";
                    button2.PerformClick();
                }
                else
                {
                    MessageBox.Show("El recibo ya fué liquidado");
                }
            }
            



        }

        public RECIBO paraImprimir()
        {
            RECIBO PI = new RECIBO();
            if (lblID.Text != ".")
            {
                
                List<CONCEPTO> CONC = new List<CONCEPTO>();
                List<EMPLEADO> emp = new List<EMPLEADO>();
                PI.Legajo = int.Parse(lblID.Text);
                PI.Ano = int.Parse(lblAno.Text);
                PI.Mes = int.Parse(lblMes.Text);
                List<CONCEPTO> aux = RECIBO.ListarConc(PI.Legajo, PI.Ano, PI.Mes);
                PI.Bruto = float.Parse(lblBruto.Text);
                PI.Conceptos = aux;
                PI.Neto = PI.Bruto + PI.TotalDescuentos();
                PI.Pagado = lblEstado.Text;

                emp = EMPLEADO.ListarID(PI.Legajo);
                foreach (EMPLEADO E in emp)
                {
                    if (E.Legajo == PI.Legajo)
                    {
                        PI.Empleado = E;
                    }
                }

            } else
            {
                MessageBox.Show("Seleccione un recibo");
                PI = null;
            }


            return PI;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            

            RECIBO prueba = new RECIBO();
            prueba = paraImprimir();
            if (prueba != null)
            {
                Recibo R = new Recibo(prueba);
                R.Visible = true;
            }
        }
    }
}
