﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace _1erParcialLUG
{
    public partial class ABMEmpleado : Form
    {
        
        public ABMEmpleado()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int filas = -1;
            if (txtCUIL.Text.Length > 0)
            {
                if (CuilExistente(txtCUIL.Text) == -1)
                {
                    if (txtNom.Text.Length > 0)
                    {
                        if (txtApe.Text.Length > 0) {
                            EMPLEADO E = new EMPLEADO();

                            E.Nombre = txtNom.Text;
                            E.Apellido = txtApe.Text;
                            E.CUIL = txtCUIL.Text;

                            filas = E.Insertar();
                            Enlazar();
                            MessageBox.Show("El empleado se dió de alta con éxito");
                            txtNom.Text = "";
                            txtApe.Text = "";
                            txtCUIL.Text = "";
                            lblLegajo.Text = ".";
                        } else
                        {
                            MessageBox.Show("Ingrese un apellido");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un nombre");
                    }

                } else
                {
                    MessageBox.Show("El CUIL ya existe");
                }
            }
            else
            {
                MessageBox.Show("Ingrese un CUIL");
            }
            
        }

        private void Enlazar ()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = EMPLEADO.Listar();
        
        }

        private int CuilExistente (string CUIL)
        {
            int ok = -1;
            List<EMPLEADO> lista = new List<EMPLEADO>();
            lista = EMPLEADO.Listar();

            foreach (EMPLEADO E in lista)
            {
                if(E.CUIL.ToString() == CUIL)
                {
                    ok = E.Legajo;
                }
            }




            return ok;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            lblLegajo.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNom.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApe.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtCUIL.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = CuilExistente(txtCUIL.Text);


            if(txtCUIL.Text.Length > 0) // CUIL
            {
                if (txtNom.Text.Length > 0) // nombre
                {
                    if (txtApe.Text.Length > 0) // apellido
                    {
                        if (id == -1 || id == int.Parse(lblLegajo.Text))
                        {
                            EMPLEADO E = new EMPLEADO();
                            E.Legajo = int.Parse(lblLegajo.Text);
                            E.Nombre = txtNom.Text;
                            E.Apellido = txtApe.Text;
                            E.CUIL = txtCUIL.Text;

                            E.Editar();
                            Enlazar();
                            txtNom.Text = "";
                            txtApe.Text = "";
                            txtCUIL.Text = "";
                            lblLegajo.Text = ".";
                        }
                        else
                        {
                            MessageBox.Show("El CUIL ya existe");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un apellido");
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese un nombre");
                }
            } else
            {
                MessageBox.Show("Ingrese un CUIL");
            }




            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lblLegajo.Text != ".")
            {
                EMPLEADO E = new EMPLEADO();
                E.Legajo = int.Parse(lblLegajo.Text);
                E.Nombre = txtNom.Text;
                E.Apellido = txtApe.Text;
                E.CUIL = txtCUIL.Text;

                E.Borrar();
                Enlazar();
                txtNom.Text = "";
                txtApe.Text = "";
                txtCUIL.Text = "";
                lblLegajo.Text = ".";
            }
            else
            {
                MessageBox.Show("Seleccione un empleado");
            }
        }
    }
}
