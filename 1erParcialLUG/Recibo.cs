﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace _1erParcialLUG
{
    public partial class Recibo : Form
    {
        public Recibo(RECIBO R)
        {
            IPRecibo = R;
            InitializeComponent();
        }

        private RECIBO IPrecibo;

        public RECIBO IPRecibo
        {
            get { return IPrecibo; }
            set { IPrecibo = value; }
        }


        private void Recibo_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {
            listBox1.DataSource = null;
            listBox1.DataSource = RECIBO.ListarItem(IPRecibo.Legajo, IPRecibo.Ano, IPRecibo.Mes);
            lblEmpleado.Text = IPRecibo.Empleado.ToString();
            lblCUIL.Text = IPRecibo.Empleado.CUIL.ToString();
            lblAno.Text = IPRecibo.Ano.ToString();
            lblMes.Text = IPRecibo.Mes.ToString();
            lblBruto.Text = IPRecibo.Bruto.ToString() + " $";
            lblDescuentos.Text = IPRecibo.TotalDescuentos().ToString() + " $";
            lblNeto.Text = IPRecibo.Neto.ToString() + " $";
            lblEstado.Text = IPRecibo.Pagado.ToString();
        }


    }
}
