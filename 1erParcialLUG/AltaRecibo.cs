﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;


namespace _1erParcialLUG
{
    public partial class AltaRecibo : Form
    {
        public AltaRecibo()
        {
            InitializeComponent();
        }

        public void EnlazarConc()
        {
            conceptos.DataSource = null;
            conceptos.DataSource = CONCEPTO.Listar();
        }

        public void EnlazarEmp()
        {
            ComboEmpleados.DataSource = null;
            ComboEmpleados.DataSource = EMPLEADO.Listar();
        }

        private void AltaRecibo_Load(object sender, EventArgs e)
        {
            EnlazarConc();
            EnlazarEmp();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int filas = -1;



            if (ComboEmpleados.SelectedItem != null) // empleado seleccionado
            {
                EMPLEADO E = (EMPLEADO)ComboEmpleados.SelectedItem;
                
                
                if (ComboAno.SelectedItem != null && int.TryParse(ComboAno.SelectedItem.ToString(), out int A) == true) // año seleccionado
                {
                    if (ComboMes.SelectedItem != null && int.TryParse(ComboMes.SelectedItem.ToString(), out int M) == true) // mes seleccionado
                    {
                        if (float.TryParse(txtBruto.Text, out float B) == true) // bruto valido
                        {
                            if (conceptos.SelectedItems.Count > 0) // algo seleccionado en lista
                            {
                                RECIBO R = new RECIBO();

                                R.Legajo = E.Legajo;
                                R.Ano = A;
                                R.Mes = M;
                                R.Bruto = B;
                                R.Pagado = "";

                                foreach (object C in conceptos.SelectedItems)
                                {
                                    R.Conceptos.Add((CONCEPTO)C);
                                }

                                filas = R.Insertar();
                                R.InsertarConceptos(R.Conceptos);
                                if (filas != -1)
                                {
                                    MessageBox.Show("El recibo se generó correctamente");
                                }
                                else
                                {
                                    MessageBox.Show("El recibo ya fue generado");
                                }
                                ComboAno.SelectedIndex = 0;
                                ComboMes.SelectedIndex = 0;
                                ComboEmpleados.SelectedIndex = 0;
                                txtBruto.Text = "";
                                conceptos.SelectedItems.Clear();
                            }
                            else
                            {
                                MessageBox.Show("Seleccione al menos un concepto");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Ingrese un sueldo bruto válido, usando (,) como separador de decimales");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Seleccione un mes");
                    }
                }
                else
                {
                    MessageBox.Show("Seleccione un año");
                }
            }
            else
            {
                MessageBox.Show("Seleccione un empleado");
            }

            
            


            



        }
    }
}
