﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace _1erParcialLUG
{
    public partial class ABMConcepto : Form
    {
        public ABMConcepto()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bool resta = true;
            bool suma = false;
            lblID.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtDesc.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtCoef.Text = (float.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString())*100).ToString();

            if (dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString() == "S")
            {
                suma = true;
                resta = false;
            }

            rbSuma.Checked = suma;
            rbResta.Checked = resta;


        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = CONCEPTO.Listar();

        }

        private void ABMConcepto_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int filasafect = -1;
            float coeficiente;

            if(float.TryParse(txtCoef.Text, out coeficiente) == true)
            {
                coeficiente = coeficiente / 100;
                if(txtDesc.Text.Length > 0)
                {
                    CONCEPTO C = new CONCEPTO();
                    C.Descripcion = txtDesc.Text;
                    C.Coeficiente = coeficiente;
                    if (rbSuma.Checked == true)
                    {
                        C.Operador = "S";
                    }
                    else
                    {
                        C.Operador = "R";
                    }
                    filasafect = C.Insertar();
                    MessageBox.Show("El concepto se creó con éxito");
                    Enlazar();
                    txtDesc.Text = "";
                    txtCoef.Text = "";
                    lblID.Text = ".";
                    rbResta.Checked = true;


                }
                else
                {
                    MessageBox.Show("Ingrese una descripcion");
                }



            }
            else
            {
                MessageBox.Show("Ingrese un coeficiente válido, utilizando (,) como separador de decimales");
            }
            if(filasafect == 0)
            {
                MessageBox.Show("No se pudo registrar el concepto");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {



            float coeficiente;

            if (float.TryParse(txtCoef.Text, out coeficiente) == true)
            {
                coeficiente = coeficiente / 100;
                if (txtDesc.Text.Length > 0)
                {
                    CONCEPTO C = new CONCEPTO();
                    C.Descripcion = txtDesc.Text;
                    C.Coeficiente = coeficiente;
                    C.ID = int.Parse(lblID.Text);
                    if (rbSuma.Checked == true)
                    {
                        C.Operador = "S";
                    }
                    else
                    {
                        C.Operador = "R";
                    }
                    C.Editar();
                    MessageBox.Show("El concepto se editó con éxito");
                    Enlazar();
                    txtDesc.Text = "";
                    txtCoef.Text = "";
                    lblID.Text = ".";
                    rbResta.Checked = true;


                }
                else
                {
                    MessageBox.Show("Ingrese una descripcion");
                }



            }
            else
            {
                MessageBox.Show("Ingrese un coeficiente válido, utilizando (,) como separador de decimales");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lblID.Text != ".")
            {
                CONCEPTO C = new CONCEPTO();
                C.ID = int.Parse(lblID.Text);
                C.Borrar();
                Enlazar();
                txtDesc.Text = "";
                txtCoef.Text = "";
                lblID.Text = ".";
                rbResta.Checked = true;
            } else
            {
                MessageBox.Show("Seleccione un concepto para borrar");
            }



        }
    }
}
