﻿namespace _1erParcialLUG
{
    partial class AltaRecibo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComboEmpleados = new System.Windows.Forms.ComboBox();
            this.ComboAno = new System.Windows.Forms.ComboBox();
            this.ComboMes = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.conceptos = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Empleado:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Año:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(12, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mes:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Conceptos:";
            // 
            // ComboEmpleados
            // 
            this.ComboEmpleados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboEmpleados.FormattingEnabled = true;
            this.ComboEmpleados.Items.AddRange(new object[] {
            " "});
            this.ComboEmpleados.Location = new System.Drawing.Point(90, 6);
            this.ComboEmpleados.Name = "ComboEmpleados";
            this.ComboEmpleados.Size = new System.Drawing.Size(159, 21);
            this.ComboEmpleados.TabIndex = 4;
            // 
            // ComboAno
            // 
            this.ComboAno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboAno.FormattingEnabled = true;
            this.ComboAno.Items.AddRange(new object[] {
            " ",
            "2020",
            "2021",
            "2022",
            "2023"});
            this.ComboAno.Location = new System.Drawing.Point(90, 35);
            this.ComboAno.Name = "ComboAno";
            this.ComboAno.Size = new System.Drawing.Size(159, 21);
            this.ComboAno.TabIndex = 5;
            // 
            // ComboMes
            // 
            this.ComboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboMes.FormattingEnabled = true;
            this.ComboMes.Items.AddRange(new object[] {
            " ",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.ComboMes.Location = new System.Drawing.Point(90, 63);
            this.ComboMes.Name = "ComboMes";
            this.ComboMes.Size = new System.Drawing.Size(159, 21);
            this.ComboMes.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(174, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 58);
            this.button1.TabIndex = 8;
            this.button1.Text = "Generar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // conceptos
            // 
            this.conceptos.FormattingEnabled = true;
            this.conceptos.Location = new System.Drawing.Point(11, 136);
            this.conceptos.Name = "conceptos";
            this.conceptos.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.conceptos.Size = new System.Drawing.Size(157, 225);
            this.conceptos.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(12, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Sueldo bruto:";
            // 
            // txtBruto
            // 
            this.txtBruto.Location = new System.Drawing.Point(90, 90);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.Size = new System.Drawing.Size(159, 20);
            this.txtBruto.TabIndex = 11;
            // 
            // AltaRecibo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 370);
            this.Controls.Add(this.txtBruto);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.conceptos);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ComboMes);
            this.Controls.Add(this.ComboAno);
            this.Controls.Add(this.ComboEmpleados);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.Name = "AltaRecibo";
            this.Text = "AltaRecibo";
            this.Load += new System.EventHandler(this.AltaRecibo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ComboEmpleados;
        private System.Windows.Forms.ComboBox ComboAno;
        private System.Windows.Forms.ComboBox ComboMes;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox conceptos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBruto;
    }
}