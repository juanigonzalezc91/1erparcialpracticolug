﻿namespace _1erParcialLUG
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barra = new System.Windows.Forms.MenuStrip();
            this.aBMEmpleadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMConceptosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarReciboToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liquidarReciboToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barra.SuspendLayout();
            this.SuspendLayout();
            // 
            // barra
            // 
            this.barra.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMEmpleadoToolStripMenuItem,
            this.aBMConceptosToolStripMenuItem,
            this.generarReciboToolStripMenuItem,
            this.liquidarReciboToolStripMenuItem});
            this.barra.Location = new System.Drawing.Point(0, 0);
            this.barra.Name = "barra";
            this.barra.Size = new System.Drawing.Size(940, 24);
            this.barra.TabIndex = 1;
            this.barra.Text = "menuStrip1";
            // 
            // aBMEmpleadoToolStripMenuItem
            // 
            this.aBMEmpleadoToolStripMenuItem.Name = "aBMEmpleadoToolStripMenuItem";
            this.aBMEmpleadoToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.aBMEmpleadoToolStripMenuItem.Text = "ABM Empleado";
            this.aBMEmpleadoToolStripMenuItem.Click += new System.EventHandler(this.aBMEmpleadoToolStripMenuItem_Click);
            // 
            // aBMConceptosToolStripMenuItem
            // 
            this.aBMConceptosToolStripMenuItem.Name = "aBMConceptosToolStripMenuItem";
            this.aBMConceptosToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.aBMConceptosToolStripMenuItem.Text = "ABM Conceptos";
            this.aBMConceptosToolStripMenuItem.Click += new System.EventHandler(this.aBMConceptosToolStripMenuItem_Click);
            // 
            // generarReciboToolStripMenuItem
            // 
            this.generarReciboToolStripMenuItem.Name = "generarReciboToolStripMenuItem";
            this.generarReciboToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.generarReciboToolStripMenuItem.Text = "Generar recibo";
            this.generarReciboToolStripMenuItem.Click += new System.EventHandler(this.generarReciboToolStripMenuItem_Click);
            // 
            // liquidarReciboToolStripMenuItem
            // 
            this.liquidarReciboToolStripMenuItem.Name = "liquidarReciboToolStripMenuItem";
            this.liquidarReciboToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.liquidarReciboToolStripMenuItem.Text = "Liquidar recibo";
            this.liquidarReciboToolStripMenuItem.Click += new System.EventHandler(this.liquidarReciboToolStripMenuItem_Click);
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 608);
            this.Controls.Add(this.barra);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.barra;
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.barra.ResumeLayout(false);
            this.barra.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip barra;
        private System.Windows.Forms.ToolStripMenuItem aBMEmpleadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMConceptosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarReciboToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liquidarReciboToolStripMenuItem;
    }
}