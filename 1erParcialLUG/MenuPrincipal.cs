﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1erParcialLUG
{
    public partial class MenuPrincipal : Form
    {
        ABMConcepto abmconcepto;
        ABMEmpleado abmempleado;
        AltaRecibo altarecibo;
        LiquidarRecibo liquidarrecibo;

        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void aBMEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(abmempleado == null)
            {
                abmempleado = new ABMEmpleado();
                abmempleado.MdiParent = this;
                abmempleado.FormClosed += Abmempleado_FormClosed;
                abmempleado.Show();
            }

        }

        private void Abmempleado_FormClosed(object sender, FormClosedEventArgs e)
        {
            abmempleado = null;
        }

        private void aBMConceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(abmconcepto == null)
            {
                abmconcepto = new ABMConcepto();
                abmconcepto.MdiParent = this;
                abmconcepto.FormClosed += Abmconcepto_FormClosed;
                abmconcepto.Show();
            }
        }

        private void Abmconcepto_FormClosed(object sender, FormClosedEventArgs e)
        {
            abmconcepto = null;
        }

        private void generarReciboToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (altarecibo == null)
            {
                altarecibo = new AltaRecibo();
                altarecibo.MdiParent = this;
                altarecibo.FormClosed += Altarecibo_FormClosed;
                altarecibo.Show();


            }

        }

        private void Altarecibo_FormClosed(object sender, FormClosedEventArgs e)
        {
            altarecibo = null;
        }

        private void liquidarReciboToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (liquidarrecibo == null)
            {
                liquidarrecibo = new LiquidarRecibo();
                liquidarrecibo.MdiParent = this;
                liquidarrecibo.FormClosed += Liquidarrecibo_FormClosed;
                liquidarrecibo.Show();
            }
                                          
        }

        private void Liquidarrecibo_FormClosed(object sender, FormClosedEventArgs e)
        {
            liquidarrecibo = null;
        }
    }
}
